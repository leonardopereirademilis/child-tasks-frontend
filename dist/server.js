"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("zone.js/dist/zone-node");
var core_1 = require("@angular/core");
var platform_server_1 = require("@angular/platform-server");
var tokens_1 = require("@nguniversal/express-engine/tokens");
var module_map_ngfactory_loader_1 = require("@nguniversal/module-map-ngfactory-loader");
var express = require("express");
var compression = require("compression");
var path_1 = require("path");
var fs_1 = require("fs");
// Faster server renders w/ Prod mode (dev mode never needed)
core_1.enableProdMode();
var http = require('http');
// Express server
var app = express();
app.use(compression());
var PORT = process.env.PORT || 8080;
var DIST_FOLDER = '';
if (process.cwd().includes('child-tasks-frontend')) {
    DIST_FOLDER = path_1.join(process.cwd(), 'dist');
}
else {
    DIST_FOLDER = path_1.join(process.cwd(), 'child-tasks-frontend', 'dist');
}
// * NOTE :: leave this as require() since this file is built Dynamically from webpack
var _a = require('./server/main'), AppServerModuleNgFactory = _a.AppServerModuleNgFactory, LAZY_MODULE_MAP = _a.LAZY_MODULE_MAP;
var template = fs_1.readFileSync('./dist/browser/index.html').toString();
app.engine('html', function (_, options, callback) {
    platform_server_1.renderModuleFactory(AppServerModuleNgFactory, {
        // Our index.html
        document: template,
        url: options.req.url,
        extraProviders: [
            module_map_ngfactory_loader_1.provideModuleMap(LAZY_MODULE_MAP),
            // make req and response accessible when angular app runs on server
            {
                provide: tokens_1.REQUEST,
                useValue: options.req
            },
            {
                provide: tokens_1.RESPONSE,
                useValue: options.req.res,
            },
        ]
    }).then(function (html) {
        callback(null, html);
    });
});
app.set('view engine', 'html');
app.set('views', path_1.join(DIST_FOLDER, 'browser'));
function wwwRedirect(req, res, next) {
    if ((req.headers.host.indexOf('www.') === -1) && (req.headers.host.indexOf('localhost') === -1)) {
        return res.redirect(301, 'https://www.' + req.headers.host + req.url);
    }
    next();
}
function indexRedirect(req, res, next) {
    if ((req.url.indexOf('/index.html') !== -1) && (req.headers.host.indexOf('localhost') === -1)) {
        var url = 'https://' + req.headers.host + req.url.replace('/index.html', '');
        return res.redirect(301, url);
    }
    next();
}
function httpsRedirect(req, res, next) {
    if ((req.protocol !== 'https') && (req.headers.host.indexOf('localhost') === -1)) {
        return res.redirect(301, 'https://' + req.headers.host + req.url);
    }
    next();
}
app.set('trust proxy', true);
app.use(wwwRedirect);
app.use(indexRedirect);
app.use(httpsRedirect);
// Server static files from /browser
app.get('*.*', express.static(path_1.join(DIST_FOLDER, 'browser'), {
    maxAge: '1y'
}));
// All regular routes use the Universal engine
app.get('*', function (req, res) {
    res.render('index', { req: req });
});
var httpServer = http.createServer(app);
// Start up the Node server
httpServer.listen(PORT, function () {
    console.log("Node Express server listening on http://localhost:" + PORT);
});
//# sourceMappingURL=server.js.map
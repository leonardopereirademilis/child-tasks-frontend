import 'zone.js/dist/zone-node';
import {enableProdMode, ValueProvider} from '@angular/core';
import {renderModuleFactory} from '@angular/platform-server';
import {REQUEST, RESPONSE} from '@nguniversal/express-engine/tokens';
import {provideModuleMap} from '@nguniversal/module-map-ngfactory-loader';
import * as express from 'express';
import * as compression from 'compression';
import {join} from 'path';
import {readFileSync} from 'fs';

// Faster server renders w/ Prod mode (dev mode never needed)
enableProdMode();

const http = require('http');

// Express server
const app = express();
app.use(compression());

const PORT = process.env.PORT || 8080;

let DIST_FOLDER = '';
if (process.cwd().includes('child-tasks-frontend')) {
  DIST_FOLDER = join(process.cwd(), 'dist');
} else {
  DIST_FOLDER = join(process.cwd(), 'child-tasks-frontend', 'dist');
}

// * NOTE :: leave this as require() since this file is built Dynamically from webpack
const {AppServerModuleNgFactory, LAZY_MODULE_MAP} = require('./server/main');

const template = readFileSync('./dist/browser/index.html').toString();
app.engine('html', (_, options, callback) => {
  renderModuleFactory(AppServerModuleNgFactory, {
    // Our index.html
    document: template,
    url: options.req.url,
    extraProviders: [
      provideModuleMap(LAZY_MODULE_MAP),
      // make req and response accessible when angular app runs on server
      {
        provide: REQUEST,
        useValue: options.req
      } as ValueProvider,
      {
        provide: RESPONSE,
        useValue: options.req.res,
      } as ValueProvider,
    ]
  }).then(html => {
    callback(null, html);
  });
});

app.set('view engine', 'html');
app.set('views', join(DIST_FOLDER, 'browser'));

function wwwRedirect(req, res, next) {
  if ((req.headers.host.indexOf('www.') === -1) && (req.headers.host.indexOf('localhost') === -1)) {
    return res.redirect(301, 'https://www.' + req.headers.host + req.url);
  }
  next();
}

function indexRedirect(req, res, next) {
  if ((req.url.indexOf('/index.html') !== -1) && (req.headers.host.indexOf('localhost') === -1)) {
    const url: string = 'https://' + req.headers.host + req.url.replace('/index.html', '');
    return res.redirect(301, url);
  }
  next();
}

function httpsRedirect(req, res, next) {
  if ((req.protocol !== 'https') && (req.headers.host.indexOf('localhost') === -1)) {
    return res.redirect(301, 'https://' + req.headers.host + req.url);
  }
  next();
}


app.set('trust proxy', true);
app.use(wwwRedirect);
app.use(indexRedirect);
app.use(httpsRedirect);

// Server static files from /browser
app.get('*.*', express.static(join(DIST_FOLDER, 'browser'), {
  maxAge: '1y'
}));

// All regular routes use the Universal engine
app.get('*', (req, res) => {
  res.render('index', {req});
});

const httpServer = http.createServer(app);

// Start up the Node server
httpServer.listen(PORT, () => {
  console.log(`Node Express server listening on http://localhost:${PORT}`);
});

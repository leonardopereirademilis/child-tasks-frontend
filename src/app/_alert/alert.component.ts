import { Component, Input, OnInit } from '@angular/core';
import { AlertService, IAlert } from './alert.service';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit {
  @Input() public alerts: Array<IAlert> = [];

  constructor(private alertService: AlertService) {}

  ngOnInit() {
    this.alerts = this.alertService.alerts;
  }

  closeAlert(alert: IAlert) {
    this.alertService.closeAlert(alert);
  }
}

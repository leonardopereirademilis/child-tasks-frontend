import {Injectable} from '@angular/core';

@Injectable()
export class AlertService {
  alerts: Array<IAlert> = [];
  tempoMinimo = 5000;
  tempoMaximo = 30000;
  tempoPorPalavra = 500;

  constructor() {
  }

  public createAlert(alertType: AlertType, message: string, dismissIn: number = 0, clearAlerts: boolean = false) {
    if (clearAlerts) {
      this.clearAlerts();
    }

    if (dismissIn === 0) {
      dismissIn = this.calculaDismisIn(message);
    }

    const alert: IAlert = {
      type: alertType,
      message,
      timeout: setTimeout(() => this.closeAlert(alert), dismissIn)
    };

    this.alerts.push(alert);
  }

  public closeAlert(alert: IAlert) {
    const index: number = this.alerts.indexOf(alert);
    clearTimeout(alert.timeout);
    this.alerts.splice(index, 1);
  }

  public createMultipleAlerts(alertType: AlertType, messages: string[], dismissIn: number = 0, clearAlerts: boolean = true) {
    if (clearAlerts) {
      this.clearAlerts();
    }

    if (dismissIn === 0) {
      messages.forEach(msg => {
        dismissIn += this.calculaDismisIn(msg);
      });
    } else {
      dismissIn = messages.length * dismissIn;
    }

    messages.forEach(msg => {
      this.createAlert(alertType, msg, dismissIn);
    });
  }

  public clearAlerts() {
    while (this.alerts.length) {
      const alert: IAlert = this.alerts.pop();
      clearTimeout(alert.timeout);
    }
  }

  private calculaDismisIn(message: string): number {
    const numeroPalavras = message.split(' ').length;
    let dismissIn = numeroPalavras * this.tempoPorPalavra;

    if (dismissIn < this.tempoMinimo) {
      dismissIn = this.tempoMinimo;
    }

    if (dismissIn > this.tempoMaximo) {
      dismissIn = this.tempoMaximo;
    }

    return dismissIn;
  }
}

export interface IAlert {
  type: AlertType;
  message: string;
  timeout: any;
}

export enum AlertType {
  SUCCESS = 'success',
  INFO = 'info',
  WARNING = 'warning',
  DANGER = 'danger'
}

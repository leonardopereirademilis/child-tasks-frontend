import {Component, OnInit} from '@angular/core';
import {AlertService, AlertType} from '../../../_alert/alert.service';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../../../users/user.service';
import {first} from 'rxjs/operators';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Board} from '../../../boards/board/board';
import {BoardService} from '../../../boards/board.service';
import {Checkout} from '../../../checkout/checkout';
import {CheckoutService} from '../../../checkout/checkout.service';
import {NgbDateParserFormatter, NgbDatepickerI18n, NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import {I18n, NgbDatepickerI18nBR} from '../../../_helpers/ngb-datepicker-i18n-br';
import {NgbDateBRParserFormatter} from '../../../_helpers/ngb-date-br-parser-formatter';

@Component({
  selector: 'app-admin-board',
  templateUrl: './admin-board.component.html',
  styleUrls: ['./admin-board.component.css'],
  providers: [
    I18n,
    {provide: NgbDatepickerI18n, useClass: NgbDatepickerI18nBR},
    {provide: NgbDateParserFormatter, useClass: NgbDateBRParserFormatter}
  ]
})
export class AdminBoardComponent implements OnInit {

  board: Board;
  oldBoard: Board = new Board();

  boardId: string;
  boardForm: FormGroup;
  internalError: string = null;
  submitted = false;

  checkouts: Checkout[] = [];
  pageSize = 20;
  page = 1;

  today: NgbDateStruct;
  birthDateStartDate: NgbDateStruct;
  startDateStartDate: NgbDateStruct;
  availableStartDateStartDate: NgbDateStruct;
  availableEndDateStartDate: NgbDateStruct;

  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private activatedRoute: ActivatedRoute,
              private userService: UserService,
              private boardService: BoardService,
              private checkoutService: CheckoutService,
              public alertService: AlertService) {
    if (this.userService.currentUser.admin && this.userService.currentUser.username !== 'lpdemilis@gmail.com') {
      this.alertService.createAlert(AlertType.DANGER, 'You ar not authorized.');
      this.router.navigate(['/']);
    }
  }

  ngOnInit() {
    const now = new Date();
    this.today = {year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate()};
    this.birthDateStartDate = {year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate()};
    this.startDateStartDate = {year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate()};

    this.internalError = null;

    this.boardId = this.activatedRoute.snapshot.paramMap.get('id');

    this.boardForm = this.formBuilder.group({
      childName: ['', Validators.required],
      childBirthDate: ['', Validators.required],
      childGender: ['', Validators.required],
      startDate: ['', Validators.required],
      boardGoal: ['', Validators.required],
      availableStartDate: [''],
      availableEndDate: [''],
      test: [''],
    });

    this.boardService.getById(this.boardId).pipe(first()).subscribe(board => {
      this.board = board;
      Object.assign(this.oldBoard, this.board);
      this.boardForm.get('childName').setValue(this.board.childName);
      this.boardForm.get('childGender').setValue(this.board.childGender);
      this.boardForm.get('boardGoal').setValue(this.board.boardGoal);
      this.boardForm.get('test').setValue(this.board.test);

      let childBirthDate: NgbDateStruct = null;
      if (this.board.childBirthDate) {
        childBirthDate = this.createDateStruct(this.board.childBirthDate);
        this.birthDateStartDate = childBirthDate;
      }
      this.boardForm.get('childBirthDate').setValue(childBirthDate);

      let startDate: NgbDateStruct = null;
      if (this.board.startDate) {
        startDate = this.createDateStruct(this.board.startDate);
        this.startDateStartDate = startDate;
      }
      this.boardForm.get('startDate').setValue(startDate);

      let availableStartDate: NgbDateStruct = null;
      if (this.board.availableStartDate) {
        availableStartDate = this.createDateStruct(this.board.availableStartDate);
        this.availableStartDateStartDate = availableStartDate;
      }
      this.boardForm.get('availableStartDate').setValue(availableStartDate);

      let availableEndDate: NgbDateStruct = null;
      if (this.board.availableEndDate) {
        availableEndDate = this.createDateStruct(this.board.availableEndDate);
        this.availableEndDateStartDate = availableEndDate;
      }
      this.boardForm.get('availableEndDate').setValue(availableEndDate);

      this.alertService.createAlert(AlertType.SUCCESS, 'Board loaded.');
    }, err => {
      this.alertService.createAlert(AlertType.DANGER, 'Board not loaded: ' + err);
    });

    this.checkoutService.getAllByBoardId(this.boardId).pipe(first()).subscribe(checkouts => {
      this.checkouts = checkouts;
      this.alertService.createAlert(AlertType.SUCCESS, 'Checkouts loaded.');
    }, err => {
      this.alertService.createAlert(AlertType.DANGER, 'Checkouts not loaded: ' + err);
    });
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.boardForm.controls;
  }

  onSubmit() {
    this.internalError = null;
    this.submitted = true;

    // stop here if form is invalid
    if (this.boardForm.invalid) {
      return;
    }

    this.board.childName = this.boardForm.value.childName;
    this.board.childGender = this.boardForm.value.childGender;
    this.board.boardGoal = this.boardForm.value.boardGoal;
    this.board.test = this.boardForm.value.test;

    if (!this.boardForm.value.availableStartDate) {
      this.board.availableStartDate = null;
    }

    if (!this.boardForm.value.availableEndDate) {
      this.board.availableEndDate = null;
    }

    this.boardService.update(this.board).pipe(first()).subscribe(() => {
      this.submitted = false;
      Object.assign(this.oldBoard, this.board);
      this.boardForm.markAsPristine();
      this.alertService.createAlert(AlertType.SUCCESS, 'Board updated.');
    }, err => {
      this.internalError = err;
    });
  }

  cancel() {
    this.board = new Board();
    Object.assign(this.board, this.oldBoard);
    this.boardForm.get('childName').setValue(this.board.childName);
    this.boardForm.get('childBirthDate').setValue(this.createDateStruct(this.board.childBirthDate));
    this.boardForm.get('childGender').setValue(this.board.childGender);
    this.boardForm.get('startDate').setValue(this.createDateStruct(this.board.startDate));
    this.boardForm.get('boardGoal').setValue(this.board.boardGoal);
    this.boardForm.get('availableStartDate').setValue(this.createDateStruct(this.board.availableStartDate));
    this.boardForm.get('availableEndDate').setValue(this.createDateStruct(this.board.availableEndDate));
    this.boardForm.get('test').setValue(this.board.test);
    this.submitted = false;
    this.birthDateStartDate = this.createDateStruct(this.board.childBirthDate);
    this.startDateStartDate = this.createDateStruct(this.board.startDate);
    this.availableStartDateStartDate = this.createDateStruct(this.board.availableStartDate);
    this.availableEndDateStartDate = this.createDateStruct(this.board.availableEndDate);
    this.boardForm.markAsPristine();
  }

  updateChildBirthDate(): void {
    if (this.boardForm && this.boardForm.value.childBirthDate) {
      this.board.childBirthDate = new Date(this.boardForm.value.childBirthDate.year,
        this.boardForm.value.childBirthDate.month - 1,
        this.boardForm.value.childBirthDate.day,
        0,
        0,
        0,
        0);
    }
  }

  updateStartDate(): void {
    if (this.boardForm && this.boardForm.value.startDate) {
      this.board.startDate = new Date(this.boardForm.value.startDate.year,
        this.boardForm.value.startDate.month - 1,
        this.boardForm.value.startDate.day,
        0,
        0,
        0,
        0);
    }
  }

  updateAvailableStartDate(): void {
    if (this.boardForm && this.boardForm.value.availableStartDate) {
      this.board.availableStartDate = new Date(this.boardForm.value.availableStartDate.year,
        this.boardForm.value.availableStartDate.month - 1,
        this.boardForm.value.availableStartDate.day,
        0,
        0,
        0,
        0);
    }
  }

  updateAvailableEndDate(): void {
    if (this.boardForm && this.boardForm.value.availableEndDate) {
      this.board.availableEndDate = new Date(this.boardForm.value.availableEndDate.year,
        this.boardForm.value.availableEndDate.month - 1,
        this.boardForm.value.availableEndDate.day,
        0,
        0,
        0,
        0);
    }
  }

  createDateStruct(date: Date): NgbDateStruct {
    let ngbDateStruct: NgbDateStruct = null;
    if (date) {
      ngbDateStruct = {
        year: new Date(date).getFullYear(),
        month: new Date(date).getMonth() + 1,
        day: new Date(date).getDate()
      };
    }

    return ngbDateStruct;
  }
}

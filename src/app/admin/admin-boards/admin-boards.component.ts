import {Component, OnInit} from '@angular/core';
import {AlertService, AlertType} from '../../_alert/alert.service';
import {Router} from '@angular/router';
import {UserService} from '../../users/user.service';
import {first} from 'rxjs/operators';
import {Board} from '../../boards/board/board';
import {BoardService} from '../../boards/board.service';

@Component({
  selector: 'app-admin-boards',
  templateUrl: './admin-boards.component.html',
  styleUrls: ['./admin-boards.component.css']
})
export class AdminBoardsComponent implements OnInit {

  boards: Board[] = [];
  pageSize = 20;
  page = 1;

  constructor(private router: Router,
              private userService: UserService,
              private boardService: BoardService,
              public alertService: AlertService) {
    if (this.userService.currentUser.admin && this.userService.currentUser.username !== 'lpdemilis@gmail.com') {
      this.alertService.createAlert(AlertType.DANGER, 'You ar not authorized.');
      this.router.navigate(['/']);
    }
  }

  ngOnInit() {
    this.boardService.getAll().pipe(first()).subscribe(boards => {
      this.boards = boards;
      this.alertService.createAlert(AlertType.SUCCESS, 'Boards loaded.');
    }, err => {
      this.alertService.createAlert(AlertType.DANGER, 'Boards not loaded: ' + err);
    });
  }

}

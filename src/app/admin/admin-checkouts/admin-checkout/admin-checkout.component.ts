import {Component, OnInit} from '@angular/core';
import {AlertService, AlertType} from '../../../_alert/alert.service';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../../../users/user.service';
import {first} from 'rxjs/operators';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Checkout} from '../../../checkout/checkout';
import {CheckoutService} from '../../../checkout/checkout.service';

@Component({
  selector: 'app-admin-checkout',
  templateUrl: './admin-checkout.component.html',
  styleUrls: ['./admin-checkout.component.css']
})
export class AdminCheckoutComponent implements OnInit {

  checkout: Checkout;
  checkoutId: string;
  checkoutForm: FormGroup;
  internalError: string = null;
  submitted = false;

  preApprovals: string;

  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private activatedRoute: ActivatedRoute,
              private userService: UserService,
              private checkoutService: CheckoutService,
              public alertService: AlertService) {
    if (this.userService.currentUser.admin && this.userService.currentUser.username !== 'lpdemilis@gmail.com') {
      this.alertService.createAlert(AlertType.DANGER, 'You ar not authorized.');
      this.router.navigate(['/']);
    }
  }

  ngOnInit() {
    this.internalError = null;

    this.checkoutId = this.activatedRoute.snapshot.paramMap.get('id');

    this.checkoutForm = this.formBuilder.group({
      status: ['', Validators.required]
    });

    this.checkoutService.getById(this.checkoutId).pipe(first()).subscribe(checkout => {
      this.checkout = checkout;
      this.checkoutForm.get('status').setValue(this.checkout.status);
      this.alertService.createAlert(AlertType.SUCCESS, 'Checkout loaded.');
    }, err => {
      this.alertService.createAlert(AlertType.DANGER, 'Checkout not loaded: ' + err);
    });

    this.checkoutService.listPreApprovals(this.checkoutId).pipe(first()).subscribe(preApprovals => {
      this.preApprovals = preApprovals;
      this.alertService.createAlert(AlertType.SUCCESS, 'PreApprovals loaded.');
    }, err => {
      this.alertService.createAlert(AlertType.DANGER, 'PreApprovals not loaded: ' + err);
    });
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.checkoutForm.controls;
  }

  onSubmit() {
    this.internalError = null;
    this.submitted = true;

    // stop here if form is invalid
    if (this.checkoutForm.invalid) {
      return;
    }

    const updatedCheckout = new Checkout();
    updatedCheckout.id = this.checkout.id;
    updatedCheckout.status = this.checkoutForm.value.status;

    this.checkoutService.update(updatedCheckout).pipe(first()).subscribe(() => {
      this.submitted = false;
      this.checkout = updatedCheckout;
      this.checkoutForm.markAsPristine();
      this.alertService.createAlert(AlertType.SUCCESS, 'Checkout updated.');
    }, err => {
      this.internalError = err;
    });
  }

  cancel() {
    this.checkoutForm.get('status').setValue(this.checkout.status);
    this.submitted = false;
    this.checkoutForm.markAsPristine();
  }

}

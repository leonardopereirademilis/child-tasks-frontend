import {Component, OnInit} from '@angular/core';
import {AlertService, AlertType} from '../../_alert/alert.service';
import {Router} from '@angular/router';
import {UserService} from '../../users/user.service';
import {first} from 'rxjs/operators';
import {Checkout} from '../../checkout/checkout';
import {CheckoutService} from '../../checkout/checkout.service';

@Component({
  selector: 'app-admin-checkouts',
  templateUrl: './admin-checkouts.component.html',
  styleUrls: ['./admin-checkouts.component.css']
})
export class AdminCheckoutsComponent implements OnInit {

  checkouts: Checkout[] = [];
  pageSize = 20;
  page = 1;

  constructor(private router: Router,
              private userService: UserService,
              private checkoutService: CheckoutService,
              public alertService: AlertService) {
    if (this.userService.currentUser.admin && this.userService.currentUser.username !== 'lpdemilis@gmail.com') {
      this.alertService.createAlert(AlertType.DANGER, 'You ar not authorized.');
      this.router.navigate(['/']);
    }
  }

  ngOnInit() {
    this.checkoutService.getAll().pipe(first()).subscribe(checkouts => {
      this.checkouts = checkouts;
      this.alertService.createAlert(AlertType.SUCCESS, 'Checkouts loaded.');
    }, err => {
      this.alertService.createAlert(AlertType.DANGER, 'Checkouts not loaded: ' + err);
    });
  }

}

import {Component, OnInit} from '@angular/core';
import {AlertService, AlertType} from '../../../_alert/alert.service';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../../../users/user.service';
import {User} from '../../../users/user';
import {first} from 'rxjs/operators';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Board} from '../../../boards/board/board';
import {BoardService} from '../../../boards/board.service';

@Component({
  selector: 'app-admin-user',
  templateUrl: './admin-user.component.html',
  styleUrls: ['./admin-user.component.css']
})
export class AdminUserComponent implements OnInit {

  user: User;
  userId: string;
  userForm: FormGroup;
  internalError: string = null;
  submitted = false;

  boards: Board[] = [];
  pageSize = 20;
  page = 1;

  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private activatedRoute: ActivatedRoute,
              private userService: UserService,
              private boardService: BoardService,
              public alertService: AlertService) {
    if (this.userService.currentUser.admin && this.userService.currentUser.username !== 'lpdemilis@gmail.com') {
      this.alertService.createAlert(AlertType.DANGER, 'You ar not authorized.');
      this.router.navigate(['/']);
    }
  }

  ngOnInit() {
    this.internalError = null;

    this.userId = this.activatedRoute.snapshot.paramMap.get('id');

    this.userForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      username: ['', [Validators.required, Validators.email]],
      admin: ['']
    });

    this.userService.getById(this.userId).pipe(first()).subscribe(user => {
      this.user = user;
      this.userForm.get('firstName').setValue(this.user.firstName);
      this.userForm.get('lastName').setValue(this.user.lastName);
      this.userForm.get('username').setValue(this.user.username);
      this.userForm.get('admin').setValue(this.user.admin);

      this.alertService.createAlert(AlertType.SUCCESS, 'User loaded.');
    }, err => {
      this.alertService.createAlert(AlertType.DANGER, 'User not loaded: ' + err);
    });

    this.boardService.getAllByUserId(this.userId).pipe(first()).subscribe(boards => {
      this.boards = boards;
      this.alertService.createAlert(AlertType.SUCCESS, 'Boards loaded.');
    }, err => {
      this.alertService.createAlert(AlertType.DANGER, 'Boards not loaded: ' + err);
    });
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.userForm.controls;
  }

  onSubmit() {
    this.internalError = null;
    this.submitted = true;

    // stop here if form is invalid
    if (this.userForm.invalid) {
      return;
    }

    const updatedUser = new User();
    updatedUser._id = this.user._id;
    updatedUser.firstName = this.userForm.value.firstName;
    updatedUser.lastName = this.userForm.value.lastName;
    updatedUser.username = this.userForm.value.username;
    updatedUser.admin = this.userForm.value.admin;

    this.userService.update(updatedUser).pipe(first()).subscribe(() => {
      this.submitted = false;
      this.user = updatedUser;
      this.userForm.markAsPristine();
      this.alertService.createAlert(AlertType.SUCCESS, 'User updated.');
    }, err => {
      this.internalError = err;
    });
  }

  cancel() {
    this.userForm.get('firstName').setValue(this.user.firstName);
    this.userForm.get('lastName').setValue(this.user.lastName);
    this.userForm.get('username').setValue(this.user.username);
    this.userForm.get('admin').setValue(this.user.admin);
    this.submitted = false;
    this.userForm.markAsPristine();
  }
}

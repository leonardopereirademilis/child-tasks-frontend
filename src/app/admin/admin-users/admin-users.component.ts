import {Component, OnInit} from '@angular/core';
import {AlertService, AlertType} from '../../_alert/alert.service';
import {Router} from '@angular/router';
import {UserService} from '../../users/user.service';
import {User} from '../../users/user';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-admin-users',
  templateUrl: './admin-users.component.html',
  styleUrls: ['./admin-users.component.css']
})
export class AdminUsersComponent implements OnInit {

  users: User[] = [];
  pageSize = 20;
  page = 1;

  constructor(private router: Router,
              private userService: UserService,
              public alertService: AlertService) {
    if (this.userService.currentUser.admin && this.userService.currentUser.username !== 'lpdemilis@gmail.com') {
      this.alertService.createAlert(AlertType.DANGER, 'You ar not authorized.');
      this.router.navigate(['/']);
    }
  }

  ngOnInit() {
    this.userService.getAll().pipe(first()).subscribe(users => {
      this.users = users;
      this.alertService.createAlert(AlertType.SUCCESS, 'Users loaded.');
    }, err => {
      this.alertService.createAlert(AlertType.DANGER, 'Users not loaded: ' + err);
    });
  }

}

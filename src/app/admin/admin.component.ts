import {Component, OnInit} from '@angular/core';
import {AlertService, AlertType} from '../_alert/alert.service';
import {Router} from '@angular/router';
import {UserService} from '../users/user.service';
import {User} from '../users/user';
import {first} from 'rxjs/operators';
import {Board} from '../boards/board/board';
import {Checkout} from '../checkout/checkout';
import {BoardService} from '../boards/board.service';
import {CheckoutService} from '../checkout/checkout.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  users: User[];
  boards: Board[];
  checkouts: Checkout[];

  constructor(private router: Router,
              private userService: UserService,
              private boardService: BoardService,
              private checkoutService: CheckoutService,
              public alertService: AlertService) {
    if (this.userService.currentUser.admin && this.userService.currentUser.username !== 'lpdemilis@gmail.com') {
      this.alertService.createAlert(AlertType.DANGER, 'You ar not authorized.');
      this.router.navigate(['/']);
    }
  }

  ngOnInit() {
    this.userService.getAll().pipe(first()).subscribe(users => {
      this.users = users;
      this.alertService.createAlert(AlertType.SUCCESS, 'Users loaded.');
    }, err => {
      this.alertService.createAlert(AlertType.DANGER, 'Users not loaded: ' + err);
    });

    this.boardService.getAll().pipe(first()).subscribe(boards => {
      this.boards = boards;
      this.alertService.createAlert(AlertType.SUCCESS, 'Boards loaded.');
    }, err => {
      this.alertService.createAlert(AlertType.DANGER, 'Boards not loaded: ' + err);
    });

    this.checkoutService.getAll().pipe(first()).subscribe(checkouts => {
      this.checkouts = checkouts;
      this.alertService.createAlert(AlertType.SUCCESS, 'Checkouts loaded.');
    }, err => {
      this.alertService.createAlert(AlertType.DANGER, 'Checkouts not loaded: ' + err);
    });
  }

}

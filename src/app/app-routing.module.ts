import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {BoardComponent} from './boards/board/board.component';
import {BoardsComponent} from './boards/boards.component';
import {LoginComponent} from './users/login/login.component';
import {RegisterComponent} from './users/register/register.component';
import {ForgotPasswordComponent} from './users/forgot-password/forgot-password.component';
import {ResetPasswordComponent} from './users/reset-password/reset-password.component';
import {AuthGuard} from './_guards/auth.guard';
import {ContactComponent} from './contact/contact.component';
import {ReturnComponent} from './checkout/return/return.component';
import {AdminComponent} from './admin/admin.component';
import {AdminUsersComponent} from './admin/admin-users/admin-users.component';
import {AdminUserComponent} from './admin/admin-users/admin-user/admin-user.component';
import {AdminBoardsComponent} from './admin/admin-boards/admin-boards.component';
import {AdminBoardComponent} from './admin/admin-boards/admin-board/admin-board.component';
import {AdminCheckoutsComponent} from './admin/admin-checkouts/admin-checkouts.component';
import {AdminCheckoutComponent} from './admin/admin-checkouts/admin-checkout/admin-checkout.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'contact', component: ContactComponent, data: {breadcrumb: 'Contact'}},
  {
    path: 'user', children: [
      {path: 'login', component: LoginComponent, data: {breadcrumb: 'Login'}},
      {path: 'register', component: RegisterComponent, data: {breadcrumb: 'Register'}},
      {path: 'forgot-password', component: ForgotPasswordComponent, data: {breadcrumb: 'Forgot Password'}},
      {path: 'reset-password', component: ResetPasswordComponent, data: {breadcrumb: 'Reset Password'}}
    ]
  },
  {
    path: 'boards', data: {breadcrumb: 'Boards'}, children: [
      {path: '', component: BoardsComponent, canActivate: [AuthGuard]},
      {path: 'board/:id', component: BoardComponent, canActivate: [AuthGuard], data: {breadcrumb: 'Board'}}
    ]
  },
  {
    path: 'checkout', children: [
      {path: 'return', component: ReturnComponent, canActivate: [AuthGuard], data: {breadcrumb: 'Return'}}
    ]
  },
  {
    path: 'admin', data: {breadcrumb: 'Admin'}, children: [
      {path: '', component: AdminComponent, canActivate: [AuthGuard]},
      {path: 'users', component: AdminUsersComponent, canActivate: [AuthGuard], data: {breadcrumb: 'Admin Users'}},
      {path: 'users/user/:id', component: AdminUserComponent, canActivate: [AuthGuard], data: {breadcrumb: 'Admin User'}},
      {path: 'boards', component: AdminBoardsComponent, canActivate: [AuthGuard], data: {breadcrumb: 'Admin Boards'}},
      {path: 'boards/board/:id', component: AdminBoardComponent, canActivate: [AuthGuard], data: {breadcrumb: 'Admin Board'}},
      {path: 'checkouts', component: AdminCheckoutsComponent, canActivate: [AuthGuard], data: {breadcrumb: 'Admin Checkouts'}},
      {path: 'checkouts/checkout/:id', component: AdminCheckoutComponent, canActivate: [AuthGuard], data: {breadcrumb: 'Admin Checkout'}}
    ]
  },
  {path: 'not-found', loadChildren: './_not-found/not-found.module#NotFoundModule', data: {breadcrumb: 'Not Found'}},
  {path: '**', redirectTo: 'not-found'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabled',
  }),
    CommonModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

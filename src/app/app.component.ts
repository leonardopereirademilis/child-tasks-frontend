import {isPlatformBrowser} from '@angular/common';
import {Component, ElementRef, Inject, OnDestroy, OnInit, PLATFORM_ID, ViewChild} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {AuthenticationService} from './users/authentication.service';
import {UserService} from './users/user.service';
import {User} from './users/user';
import {BoardService} from './boards/board.service';
import {AlertService, AlertType} from './_alert/alert.service';
import {Board} from './boards/board/board';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  currentUser: User;

  @ViewChild('toggleButton', {static: false}) toggleButton: ElementRef;
  @ViewChild('navbarCollapse', {static: false}) navbarCollapse: ElementRef;

  constructor(private router: Router,
              private activatedRoute: ActivatedRoute,
              private authenticationService: AuthenticationService,
              private userService: UserService,
              private translate: TranslateService,
              public boardService: BoardService,
              public alertService: AlertService,
              @Inject(PLATFORM_ID) private platformId: any) {
    if (isPlatformBrowser(this.platformId)) {
      this.userService.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
        this.userService.currentUser = user;
        this.currentUser = this.userService.currentUser;
        if (this.currentUser) {
          boardService.loadAllBoards(this.currentUser._id);
        }
      });
    }
    translate.setDefaultLang('pt');

    router.events.subscribe(e => {
      if (e instanceof NavigationEnd) {
        if (this.navbarCollapse.nativeElement.classList.contains('show')) {
          this.toggleButton.nativeElement.click();
        }
      }
    });
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    if (this.userService.currentUserSubscription) {
      this.userService.currentUserSubscription.unsubscribe();
    }
  }

  logout() {
    this.userService.currentUser = null;
    this.currentUser = null;
    this.authenticationService.logout();
    this.router.navigate(['/']);
  }

  isActive(route: string): boolean {
    if ((route === '/boards/board') && (this.router.routerState.snapshot.url.includes(route))) {
      return true;
    }

    return this.router.routerState.snapshot.url === route;
  }

  isAvailable(board: Board): boolean {
    return new Date(board.availableEndDate) >= new Date();
  }

  copy() {
    const el = document.createElement('textarea');
    el.value = 'contato@quadrodetarefas.com.br';
    el.setAttribute('readonly', '');
    el.style.position = 'absolute';
    el.style.left = '-9999px';
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
    this.alertService.createAlert(AlertType.SUCCESS, 'E-mail copied to clipboard');
  }

}

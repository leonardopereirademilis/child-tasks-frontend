import {InjectionToken} from '@angular/core';

export class AppConfig {
  apiUrl: string;
  pagseguroUrl: string;
  MON3_BRL10: string;
}

export let APP_CONFIG = new InjectionToken<AppConfig>('app.config');

import {BrowserModule} from '@angular/platform-browser';
import {LOCALE_ID, NgModule} from '@angular/core';
import localePt from '@angular/common/locales/pt';
import {AppComponent} from './app.component';
import {StatusComponent} from './boards/board/task/status/status.component';
import {AppRoutingModule} from './app-routing.module';
import {HomeComponent} from './home/home.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {BoardComponent} from './boards/board/board.component';
import {APP_CONFIG} from './app.config';
import {environment} from '../environments/environment';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BoardsComponent} from './boards/boards.component';
import {NewBoardComponent} from './boards/new-board/new-board.component';
import {LoginComponent} from './users/login/login.component';
import {RegisterComponent} from './users/register/register.component';
import {AlertComponent} from './_alert/alert.component';
import {ForgotPasswordComponent} from './users/forgot-password/forgot-password.component';
import {ResetPasswordComponent} from './users/reset-password/reset-password.component';
import {JwtInterceptor} from './_helpers/jwt.interceptor';
import {ErrorInterceptor} from './_helpers/error.interceptor';
import {ConfirmationPopoverModule} from 'angular-confirmation-popover';
import {NgxUiLoaderHttpModule, NgxUiLoaderModule, NgxUiLoaderRouterModule} from 'ngx-ui-loader';
import {RouterModule} from '@angular/router';
import {TransferHttpCacheModule} from '@nguniversal/common';
import {CommonModule, registerLocaleData} from '@angular/common';
import {NewTaskComponent} from './boards/board/new-task/new-task.component';
import {AlertService} from './_alert/alert.service';
import {TrophyComponent} from './boards/board/task/trophy/trophy.component';
import {ContactComponent} from './contact/contact.component';
import {CheckoutComponent} from './checkout/checkout.component';
import {ReturnComponent} from './checkout/return/return.component';
import {AdminComponent} from './admin/admin.component';
import {AdminUsersComponent} from './admin/admin-users/admin-users.component';
import {AdminUserComponent} from './admin/admin-users/admin-user/admin-user.component';
import {AdminBoardsComponent} from './admin/admin-boards/admin-boards.component';
import {AdminBoardComponent} from './admin/admin-boards/admin-board/admin-board.component';
import {AdminCheckoutsComponent} from './admin/admin-checkouts/admin-checkouts.component';
import {AdminCheckoutComponent} from './admin/admin-checkouts/admin-checkout/admin-checkout.component';
import {AuthServiceConfig, FacebookLoginProvider, GoogleLoginProvider, SocialLoginModule} from 'angularx-social-login';
import { RecaptchaModule } from 'ng-recaptcha';
import { ServiceWorkerModule } from '@angular/service-worker';

registerLocaleData(localePt, 'pt');

export const APP_ID = 'child-tasks-frontend';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

const config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider('597273733180-68di1h6ktu3lnflrn6belb2dcrf1e4f8.apps.googleusercontent.com')
  },
  {
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider('521670108372989')
  }
]);

export function provideConfig() {
  return config;
}

@NgModule({
  declarations: [
    AppComponent,
    StatusComponent,
    HomeComponent,
    BoardComponent,
    BoardsComponent,
    NewBoardComponent,
    LoginComponent,
    RegisterComponent,
    AlertComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
    NewTaskComponent,
    TrophyComponent,
    ContactComponent,
    CheckoutComponent,
    ReturnComponent,
    AdminComponent,
    AdminUsersComponent,
    AdminUserComponent,
    AdminBoardsComponent,
    AdminBoardComponent,
    AdminCheckoutsComponent,
    AdminCheckoutComponent
  ],
  imports: [
    SocialLoginModule,
    BrowserModule.withServerTransition({appId: APP_ID}),
    RouterModule.forRoot([
      {path: '', component: HomeComponent, pathMatch: 'full'},
      {path: 'lazy', loadChildren: './lazy/lazy.module#LazyModule'},
      {path: 'lazy/nested', loadChildren: './lazy/lazy.module#LazyModule'}
    ]),
    TransferHttpCacheModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    NgxUiLoaderModule,
    NgxUiLoaderHttpModule,
    NgxUiLoaderRouterModule,
    ConfirmationPopoverModule.forRoot({
      confirmButtonType: 'danger' // set defaults here
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    }),
    RecaptchaModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [
    AlertService,
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},
    {provide: APP_CONFIG, useValue: environment.APP_CONFIG_IMPL},
    {provide: LOCALE_ID, useValue: 'pt'},
    {provide: 'LOCALSTORAGE', useFactory: getLocalStorage},
    {provide: AuthServiceConfig, useFactory: provideConfig}
  ],
  exports: [
    HttpClientModule,
    CommonModule,
    FormsModule,
    NgbModule,
    RouterModule,
    ConfirmationPopoverModule,
    AlertComponent
  ],
  bootstrap: [AppComponent]
})

export class AppModule {
}

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

export function getLocalStorage() {
  return (typeof window !== 'undefined') ? window.localStorage : null;
}

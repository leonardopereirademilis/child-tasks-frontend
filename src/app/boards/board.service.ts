import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {APP_CONFIG, AppConfig} from '../app.config';
import {Board} from './board/board';
import {first} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BoardService {
  apiUrl: string;

  boards: Board[] = [];
  myBoards: Board[] = [];
  currentBoard: Board = null;

  constructor(private http: HttpClient,
              private router: Router,
              @Inject(APP_CONFIG) config: AppConfig) {
    this.apiUrl = config.apiUrl;
  }

  getAll() {
    return this.http.get<Board[]>(`${this.apiUrl}/boards`);
  }

  getById(id: string) {
    return this.http.get<Board>(`${this.apiUrl}/boards/${id}`);
  }

  getAllByUserId(id: string) {
    return this.http.get<Board[]>(`${this.apiUrl}/boards/user/${id}`);
  }

  create(board: Board) {
    return this.http.post<Board>(`${this.apiUrl}/boards`, board);
  }

  update(board: Board) {
    return this.http.put(`${this.apiUrl}/boards/${board.id}`, board);
  }

  delete(id: string) {
    return this.http.delete(`${this.apiUrl}/boards/${id}`);
  }

  loadAllBoards(userId) {
    this.getAllByUserId(userId).pipe(first()).subscribe(boards => {
      this.boards = boards;
      this.myBoards = boards;
    });
  }

  selectCurrentBoard(board: Board) {
    this.currentBoard = board;
    this.router.navigate(['board', board.id]);
  }

  updateBoard(boardId: string) {
    this.router.navigate(['board', boardId]);
  }

  getWeekPrize(weekNumber: number, year: number, boardId: string) {
    return this.http.post<number>(`${this.apiUrl}/boards/weekPrize`, {weekNumber, year, boardId});
  }

  getMonthPrize(month: number, year: number, boardId: string) {
    return this.http.post<number>(`${this.apiUrl}/boards/monthPrize`, {month, year, boardId});
  }

  getYearPrize(year: number, boardId: string) {
    return this.http.post<number>(`${this.apiUrl}/boards/yearPrize`, {year, boardId});
  }
}

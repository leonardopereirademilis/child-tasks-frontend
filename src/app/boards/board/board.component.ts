import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {BoardService} from '../board.service';
import {first} from 'rxjs/operators';
import {ActivatedRoute, Router} from '@angular/router';
import * as moment from 'moment';
import {StatusService} from './task/status/status.service';
import {Status, StatusValue} from './task/status/status';
import {TaskService} from './task/task.service';
import {AlertService, AlertType} from '../../_alert/alert.service';
import {Task} from './task/task';
import {NewTaskComponent} from './new-task/new-task.component';
import {TranslateService} from '@ngx-translate/core';
import {Board} from './board';
import {UserService} from '../../users/user.service';

declare var $: any;

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})
export class BoardComponent implements OnInit, AfterViewInit {

  constructor(private activatedRoute: ActivatedRoute,
              private alertService: AlertService,
              private router: Router,
              private translateService: TranslateService,
              public boardService: BoardService,
              private taskService: TaskService,
              private userService: UserService,
              private statusService: StatusService) {
  }

  boardId: string;
  today: Date;
  days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
  months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  week: Date[] = [];
  monthName1: string;
  monthName2: string;
  year1: number;
  year2: number;
  tasksLoaded = false;

  weekPrize = 0;
  monthPrize = 0;
  yearPrize = 0;

  @ViewChild('newTaskComponent', {static: false}) newTaskComponent: NewTaskComponent;
  @ViewChild('divStatusTable', {static: false}) divStatusTable: ElementRef;

  static getPrize(task: Task): number {
    return Math.round(task.prize * 100);
  }

  ngOnInit() {
    this.boardId = this.activatedRoute.snapshot.paramMap.get('id');

    this.today = new Date();
    this.today.setHours(0);
    this.today.setMinutes(0);
    this.today.setSeconds(0);
    this.today.setMilliseconds(0);
    this.loadAllTasks();
  }

  ngAfterViewInit() {
    this.setScroll();
  }

  private loadAllTasks() {
    this.tasksLoaded = false;

    this.boardService.getById(this.boardId)
      .pipe(first())
      .subscribe(
        board => {
          this.boardService.currentBoard = board;

          // @ts-ignore
          if (this.boardService.currentBoard.user !== this.userService.currentUser._id) {
            this.alertService.createAlert(AlertType.DANGER, 'This board is unavailable.');
            this.router.navigate(['/']);
          }

          if (!this.isAvailable(this.boardService.currentBoard)) {
            this.alertService.createAlert(AlertType.DANGER, 'This board is unavailable.');
            this.router.navigate(['/boards']);
          }

          this.setWeek(this.today);
          this.getTasks();
        },
        error => {
          this.tasksLoaded = true;
          this.alertService.createAlert(AlertType.DANGER, error);
        });
  }

  private getTasks() {
    if (this.boardService.currentBoard) {
      this.taskService.getAllByBoardId(this.boardService.currentBoard.id)
        .pipe(first())
        .subscribe(
          tasks => {
            this.boardService.currentBoard.taskList = tasks;
            if (!this.boardService.currentBoard.taskList) {
              this.tasksLoaded = true;
            }

            let translatedTasks = 0;
            this.boardService.currentBoard.taskList.forEach(task => {
              task.statusList = [];
              this.week.forEach(weekday => {
                const status = new Status();
                status.date = weekday;
                task.statusList.push(status);
              });
              this.translateService.get(task.taskName).subscribe(value => {
                task.taskName = value;
                translatedTasks++;
                if (translatedTasks === this.boardService.currentBoard.taskList.length) {
                  this.boardService.currentBoard.taskList = this.boardService.currentBoard.taskList.sort((a, b) => {
                    if (a.taskName < b.taskName) {
                      return -1;
                    } else if (a.taskName > b.taskName) {
                      return 1;
                    } else {
                      return 0;
                    }
                  });
                }

                this.getStatusList(task);
              });
            });
          },
          error => {
            this.tasksLoaded = true;
            this.alertService.createAlert(AlertType.DANGER, error);
          });
    }
  }

  getStatusList(task: Task) {
    this.statusService.getAllByTaskIdAndWeekNumber(task.id, this.boardService.currentBoard.weekNumber)
      .pipe(first())
      .subscribe(statusListDB => {
        this.week.forEach((weekDay, index) => {
          const statusDB: Status = statusListDB.find(sdb => new Date(sdb.date).getDay() === index);
          const status = new Status();
          if (!statusDB) {
            status.date = weekDay;
            status.weekNumber = this.boardService.currentBoard.weekNumber;
            status.statusValue = null;
          } else {
            status.id = statusDB.id;
            status.date = new Date(statusDB.date);
            status.weekNumber = statusDB.weekNumber;
            status.statusValue = statusDB.statusValue;
            status.feedback = statusDB.feedback;
          }

          task.statusList[index] = status;
          task = this.updateTask(task);
          this.tasksLoaded = true;
        });
      });
    this.updatePrize();
  }

  updateTask(task: Task): Task {
    const currentWeek = moment().week();
    let totalStatus = 0;
    if (task.sunday) {
      totalStatus++;
    }
    if (task.monday && (this.today.getDay() >= 1 || currentWeek > this.boardService.currentBoard.weekNumber)) {
      totalStatus++;
    }
    if (task.tuesday && (this.today.getDay() >= 2 || currentWeek > this.boardService.currentBoard.weekNumber)) {
      totalStatus++;
    }
    if (task.wednesday && (this.today.getDay() >= 3 || currentWeek > this.boardService.currentBoard.weekNumber)) {
      totalStatus++;
    }
    if (task.thursday && (this.today.getDay() >= 4 || currentWeek > this.boardService.currentBoard.weekNumber)) {
      totalStatus++;
    }
    if (task.friday && (this.today.getDay() >= 5 || currentWeek > this.boardService.currentBoard.weekNumber)) {
      totalStatus++;
    }
    if (task.saturday && (this.today.getDay() >= 6 || currentWeek > this.boardService.currentBoard.weekNumber)) {
      totalStatus++;
    }

    const successStatus = task.statusList.filter(status => status.statusValue === StatusValue.SUCCESS).length;
    const warningStatus = task.statusList.filter(status => status.statusValue === StatusValue.WARNING).length;
    const dangerStatus = task.statusList.filter(status => status.statusValue === StatusValue.DANGER).length;
    const undefinedStatus = totalStatus - successStatus - warningStatus - dangerStatus;

    totalStatus = totalStatus - undefinedStatus;

    if (totalStatus && totalStatus > 0) {
      task.prize = ((successStatus * 2) + warningStatus) / (totalStatus * 2);
      task.disabled = false;
    } else {
      task.prize = 0;
      task.disabled = true;
    }

    return task;
  }

  setWeek(date: Date) {
    const weekStart = new Date();
    weekStart.setHours(0);
    weekStart.setMinutes(0);
    weekStart.setSeconds(0);
    weekStart.setMilliseconds(0);
    weekStart.setDate(date.getDate() - date.getDay());

    this.boardService.currentBoard.weekNumber = moment(weekStart).week();

    for (let i = 0; i < 7; i++) {
      let day = moment().set({
        year: weekStart.getFullYear(),
        month: weekStart.getMonth(),
        day: weekStart.getDay(),
        hour: 0,
        minute: 0,
        second: 0,
        millisecond: 0
      });
      day = day.add(i, 'day');
      this.week[i] = day.toDate();
    }

    this.updatePrize();
    this.setCalendarName();
  }

  previousWeek() {
    this.week.forEach(day => {
      day.setDate(day.getDate() - 7);
    });
    this.boardService.currentBoard.weekNumber--;
    this.setCalendarName();
    this.getTasks();
    this.setScroll();
    this.updatePrize();
  }

  nextWeek() {
    this.week.forEach(day => {
      day.setDate(day.getDate() + 7);
    });
    this.boardService.currentBoard.weekNumber++;
    this.setCalendarName();
    this.getTasks();
    this.setScroll();
    this.updatePrize();
  }

  isToday(date: Date): boolean {
    return date.getDate() === this.today.getDate()
      && date.getMonth() === this.today.getMonth()
      && date.getFullYear() === this.today.getFullYear();
  }

  getWeekDay(date: Date): string {
    return this.days[date.getDay()];
  }

  getFormatedDay(date: Date): string {
    return date.toDateString();
  }

  getCardStatus(task: Task, date: Date): string {
    let cardStatus = 'enabled';

    switch (date.getDay()) {
      case 0:
        if (!task.sunday) {
          cardStatus = 'disabled';
        }
        break;
      case 1:
        if (!task.monday) {
          cardStatus = 'disabled';
        }
        break;
      case 2:
        if (!task.tuesday) {
          cardStatus = 'disabled';
        }
        break;
      case 3:
        if (!task.wednesday) {
          cardStatus = 'disabled';
        }
        break;
      case 4:
        if (!task.thursday) {
          cardStatus = 'disabled';
        }
        break;
      case 5:
        if (!task.friday) {
          cardStatus = 'disabled';
        }
        break;
      case 6:
        if (!task.saturday) {
          cardStatus = 'disabled';
        }
        break;
    }

    if ((cardStatus !== 'disabled') && (date > this.today)) {
      cardStatus = 'future';
    }

    if (date < new Date(task.startDate)) {
      cardStatus = 'disabled';
    }

    if (task.endDate && date > new Date(task.endDate)) {
      cardStatus = 'disabled';
    }

    return cardStatus;
  }

  editTask(task: Task) {
    this.newTaskComponent.editTask(task);
  }

  deleteTask(task: Task) {
    this.taskService.delete(task.id).pipe(first()).subscribe(() => {
      this.loadAllTasks();
      this.alertService.createAlert(AlertType.SUCCESS, 'Task deleted.');
    }, err => {
      this.alertService.createAlert(AlertType.DANGER, 'Task not deleted. Error: ' + err);
    });
  }

  getAge(date: Date): number {
    return moment().diff(date, 'years');
  }

  updatePrize() {
    this.setWeekPrize();
    this.setMonthPrize();
    this.setYearPrize();
  }

  setWeekPrize() {
    this.boardService.getWeekPrize(this.boardService.currentBoard.weekNumber, this.week[0].getFullYear(), this.boardId)
      .pipe(first())
      .subscribe(
        weekPrize => {
          this.weekPrize = weekPrize;
        },
        error => {
          this.alertService.createAlert(AlertType.DANGER, error);
        });
    // let boardPrize = 0;
    // let disabledTask = 0;
    //
    // if (this.boardService.currentBoard.taskList) {
    //   this.boardService.currentBoard.taskList.forEach(task => {
    //     boardPrize += task.prize;
    //     if (task.disabled) {
    //       disabledTask++;
    //     }
    //   });
    //
    //   this.weekPrize = Math.round((boardPrize / (this.boardService.currentBoard.taskList.length - disabledTask)) * 100);
    // }
  }

  setMonthPrize() {
    this.boardService.getMonthPrize(this.week[0].getMonth(), this.week[0].getFullYear(), this.boardId)
      .pipe(first())
      .subscribe(
        monthPrize => {
          this.monthPrize = monthPrize;
        },
        error => {
          this.alertService.createAlert(AlertType.DANGER, error);
        });
  }

  setYearPrize() {
    this.boardService.getYearPrize(this.week[0].getFullYear(), this.boardId)
      .pipe(first())
      .subscribe(
        yearPrize => {
          this.yearPrize = yearPrize;
        },
        error => {
          this.alertService.createAlert(AlertType.DANGER, error);
        });
  }

  setCalendarName() {
    this.monthName1 = null;
    this.monthName2 = null;
    this.year1 = null;
    this.year2 = null;

    this.monthName1 = this.months[this.week[0].getMonth()];

    if (this.week[0].getMonth() !== this.week[6].getMonth()) {
      this.monthName2 = this.months[this.week[6].getMonth()];
    }

    this.year1 = this.week[0].getFullYear();

    if (this.week[0].getFullYear() !== this.week[6].getFullYear()) {
      this.year2 = this.week[6].getFullYear();
    }
  }

  setScroll() {
    setTimeout(() => {
      $('#divStatusTable').scrollLeft($('#date_' + moment().day()).outerWidth() * moment().day());
    }, 300);
  }

  isAvailable(board: Board): boolean {
    return new Date(board.availableEndDate) >= new Date();
  }

}

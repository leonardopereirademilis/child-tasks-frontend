﻿import {User} from '../../users/user';
import {Task} from './task/task';
import {Checkout} from '../../checkout/checkout';

export class Board {
  _id: string;
  id: string;
  childName: string;
  childBirthDate: Date;
  childGender: string = null;
  user: User;
  startDate: Date;
  boardGoal = 80;

  availableStartDate: Date;
  availableEndDate: Date;
  test: boolean;

  weekNumber: number;
  taskList: Task[];

  checkouts: Checkout[];
}

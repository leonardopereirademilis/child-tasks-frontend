import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {
  NgbActiveModal,
  NgbDateParserFormatter,
  NgbDatepickerI18n,
  NgbDateStruct,
  NgbModal
} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {first} from 'rxjs/operators/first';
import {Task} from '../task/task';
import {TaskService} from '../task/task.service';
import {Board} from '../board';
import {Router} from '@angular/router';
import {TaskType} from '../task/taskType';
import {I18n, NgbDatepickerI18nBR} from '../../../_helpers/ngb-datepicker-i18n-br';
import {NgbDateBRParserFormatter} from '../../../_helpers/ngb-date-br-parser-formatter';
import {AlertService, AlertType} from '../../../_alert/alert.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-new-task',
  templateUrl: './new-task.component.html',
  styleUrls: ['./new-task.component.css'],
  providers: [
    I18n,
    {provide: NgbDatepickerI18n, useClass: NgbDatepickerI18nBR},
    {provide: NgbDateParserFormatter, useClass: NgbDateBRParserFormatter}
  ]
})
export class NewTaskComponent implements OnInit {
  @Input() currentBoard: Board;

  today: NgbDateStruct;
  startDateMinDate: NgbDateStruct;
  startDateStartDate: NgbDateStruct;
  startDateMaxDate: NgbDateStruct;
  endDateStartDate: NgbDateStruct;
  endDateMinDate: NgbDateStruct;
  registerForm: FormGroup;
  newTask: Task = new Task();
  taskTypes: TaskType[];
  submitted = false;
  internalError: string = null;
  days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
  weekDaysError = false;
  isEditing = false;

  @Output() loadAllTasks = new EventEmitter();

  @ViewChild('content', {static: false}) content: ElementRef;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private translateService: TranslateService,
              private alertService: AlertService,
              private modalService: NgbModal,
              private taskService: TaskService) {
  }

  ngOnInit() {
    const now = new Date();
    this.today = {year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate()};
    const boardStartDate = new Date(this.currentBoard.startDate);
    this.startDateMinDate = {
      year: boardStartDate.getFullYear(),
      month: boardStartDate.getMonth() + 1,
      day: boardStartDate.getDate()
    };
    this.startDateStartDate = {year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate()};
    this.startDateMaxDate = null;
    this.endDateStartDate = {year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate()};
    this.endDateMinDate = {year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate()};

    this.taskService.getAllTaskType().pipe(first()).subscribe(taskTypes => {
      this.taskTypes = taskTypes;

      let translatedTasks = 0;
      this.taskTypes.forEach(taskType => {
        this.translateService.get(taskType.taskTypeName).subscribe(value => {
          taskType.taskTypeName = value;
          translatedTasks++;
          if (translatedTasks === this.taskTypes.length) {
            this.taskTypes = this.taskTypes.sort((a, b) => {
              if (a.taskTypeName < b.taskTypeName) {
                return -1;
              } else if (a.taskTypeName > b.taskTypeName) {
                return 1;
              } else {
                return 0;
              }
            });
          }
        });
      });

    });
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.registerForm.controls;
  }

  editTask(task: Task) {
    this.isEditing = true;
    this.newTask = new Task();
    Object.assign(this.newTask, task);
    this.open(this.content);
  }

  open(content: any) {
    const boardStartDate = new Date(this.currentBoard.startDate);
    this.startDateMinDate = {
      year: boardStartDate.getFullYear(),
      month: boardStartDate.getMonth() + 1,
      day: boardStartDate.getDate()
    };
    this.modalService.dismissAll();
    this.submitted = false;
    this.internalError = null;

    this.registerForm = this.formBuilder.group({
      taskType: ['', Validators.required],
      taskName: ['', Validators.required],
      sunday: [],
      monday: [],
      tuesday: [],
      wednesday: [],
      thursday: [],
      friday: [],
      saturday: [],
      startDate: ['', Validators.required],
      endDate: [],
      taskGoal: ['', [Validators.required, Validators.min(1), Validators.max(100)]]
    });

    if (!this.isEditing) {
      this.newTask = new Task();
    }

    this.newTask.board = this.currentBoard;
    if (this.newTask.taskType) {
      this.registerForm.get('taskType').setValue(this.newTask.taskType.id);
    } else {
      this.registerForm.get('taskType').setValue(null);
    }
    this.registerForm.get('taskName').setValue(this.newTask.taskName);
    this.registerForm.get('sunday').setValue(this.newTask.sunday);
    this.registerForm.get('monday').setValue(this.newTask.monday);
    this.registerForm.get('tuesday').setValue(this.newTask.tuesday);
    this.registerForm.get('wednesday').setValue(this.newTask.wednesday);
    this.registerForm.get('thursday').setValue(this.newTask.thursday);
    this.registerForm.get('friday').setValue(this.newTask.friday);
    this.registerForm.get('saturday').setValue(this.newTask.saturday);
    this.registerForm.get('startDate').setValue(null);
    this.registerForm.get('endDate').setValue(null);
    this.registerForm.get('taskGoal').setValue(this.newTask.taskGoal);

    if (this.newTask.startDate) {
      this.startDateStartDate = {
        year: new Date(this.newTask.startDate).getFullYear(),
        month: new Date(this.newTask.startDate).getMonth() + 1,
        day: new Date(this.newTask.startDate).getDate()
      };

      this.endDateMinDate = {
        year: new Date(this.newTask.startDate).getFullYear(),
        month: new Date(this.newTask.startDate).getMonth() + 1,
        day: new Date(this.newTask.startDate).getDate()
      };
    }
    this.registerForm.get('startDate').setValue(this.startDateStartDate);

    if (this.newTask.endDate) {
      this.endDateStartDate = {
        year: new Date(this.newTask.endDate).getFullYear(),
        month: new Date(this.newTask.endDate).getMonth() + 1,
        day: new Date(this.newTask.endDate).getDate()
      };

      this.startDateMaxDate = {
        year: new Date(this.newTask.endDate).getFullYear(),
        month: new Date(this.newTask.endDate).getMonth() + 1,
        day: new Date(this.newTask.endDate).getDate()
      };
      this.registerForm.get('endDate').setValue(this.endDateStartDate);
    }

    this.modalService.open(content, {size: 'lg', centered: true}).result.then((result) => {
    }, (reason) => {
      if (this.isEditing) {
        this.alertService.createAlert(AlertType.INFO, 'Task not updated.');
      } else {
        this.alertService.createAlert(AlertType.INFO, 'Task not created.');
      }
      this.submitted = false;
      this.isEditing = false;
    });
  }

  onSubmit(modal: NgbActiveModal) {
    this.internalError = null;
    this.weekDaysError = false;
    this.submitted = true;
    this.checkWeekDays();

    // stop here if form is invalid
    if (this.registerForm.invalid || this.weekDaysError) {
      return;
    }

    this.newTask.board = this.currentBoard;
    this.newTask.taskType = this.registerForm.value.taskType;
    this.newTask.taskName = this.registerForm.value.taskName;
    this.newTask.sunday = this.registerForm.value.sunday;
    this.newTask.monday = this.registerForm.value.monday;
    this.newTask.tuesday = this.registerForm.value.tuesday;
    this.newTask.wednesday = this.registerForm.value.wednesday;
    this.newTask.thursday = this.registerForm.value.thursday;
    this.newTask.friday = this.registerForm.value.friday;
    this.newTask.saturday = this.registerForm.value.saturday;
    this.newTask.taskGoal = this.registerForm.value.taskGoal;

    if (!this.registerForm.value.endDate) {
      this.newTask.endDate = null;
    }

    if (this.isEditing) {
      this.taskService.update(this.newTask).pipe(first()).subscribe(() => {
        this.submitted = false;
        this.isEditing = false;
        modal.close();
        this.loadAllTasks.emit();
        this.alertService.createAlert(AlertType.SUCCESS, 'Task updated.');
      }, err => {
        this.internalError = err;
      });
    } else {
      this.taskService.create(this.newTask).pipe(first()).subscribe(() => {
        this.submitted = false;
        this.isEditing = false;
        modal.close();
        this.loadAllTasks.emit();
        this.alertService.createAlert(AlertType.SUCCESS, 'Task created.');
      }, err => {
        this.internalError = err;
      });
    }
  }

  updateTaskType(event) {
    this.newTask.taskType = this.taskTypes.find(taskType => taskType.id === event.target.value);
    this.newTask.taskName = this.newTask.taskType.taskTypeName;
    this.registerForm.get('taskName').setValue(this.newTask.taskType.taskTypeName);
  }

  checkWeekDays() {
    this.weekDaysError = !this.registerForm.value.sunday
      && !this.registerForm.value.monday
      && !this.registerForm.value.tuesday
      && !this.registerForm.value.wednesday
      && !this.registerForm.value.thursday
      && !this.registerForm.value.friday
      && !this.registerForm.value.saturday;
  }

  updateStartDate(): void {
    this.newTask.startDate = new Date(this.registerForm.value.startDate.year,
      this.registerForm.value.startDate.month - 1,
      this.registerForm.value.startDate.day,
      0, 0, 0, 0);
    if (this.newTask.startDate) {
      this.startDateStartDate = {
        year: new Date(this.newTask.startDate).getFullYear(),
        month: new Date(this.newTask.startDate).getMonth() + 1,
        day: new Date(this.newTask.startDate).getDate()
      };

      this.endDateMinDate = {
        year: new Date(this.newTask.startDate).getFullYear(),
        month: new Date(this.newTask.startDate).getMonth() + 1,
        day: new Date(this.newTask.startDate).getDate()
      };
    }
  }

  updateEndDate(): void {
    this.newTask.endDate = new Date(this.registerForm.value.endDate.year,
      this.registerForm.value.endDate.month - 1,
      this.registerForm.value.endDate.day,
      0, 0, 0, 0);
    if (this.newTask.endDate) {
      this.endDateStartDate = {
        year: new Date(this.newTask.endDate).getFullYear(),
        month: new Date(this.newTask.endDate).getMonth() + 1,
        day: new Date(this.newTask.endDate).getDate()
      };

      this.startDateMaxDate = {
        year: new Date(this.newTask.endDate).getFullYear(),
        month: new Date(this.newTask.endDate).getMonth() + 1,
        day: new Date(this.newTask.endDate).getDate()
      };
    }
  }

}

import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {StatusService} from './status.service';
import {AlertService, AlertType} from '../../../../_alert/alert.service';
import {Task} from '../task';
import {FormBuilder, FormGroup} from '@angular/forms';
import {first} from 'rxjs/operators';
import {Status, StatusValue} from './status';

@Component({
  selector: 'app-status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.css']
})
export class StatusComponent implements OnInit {

  SUCCESS = StatusValue.SUCCESS;
  WARNING = StatusValue.WARNING;
  DANGER = StatusValue.DANGER;

  @Input() currentTask: Task;
  @Input() currentStatus: Status;
  @Input() cardStatus: string;
  @Input() isToday: boolean;

  registerForm: FormGroup;
  submitted = false;
  internalError: string = null;

  @Output() loadStatusList = new EventEmitter<Task>();

  statusOld: Status;

  constructor(private formBuilder: FormBuilder,
              private modalService: NgbModal,
              private statusService: StatusService,
              private alertService: AlertService) {
  }

  ngOnInit() {
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.registerForm.controls;
  }

  open(content) {
    this.statusOld = new Status();
    if (this.currentStatus) {
      Object.assign(this.statusOld, this.currentStatus);
    }
    this.modalService.dismissAll();
    this.submitted = false;
    this.internalError = null;

    this.registerForm = this.formBuilder.group({
      statusValue: [''],
      feedback: ['']
    });

    this.registerForm.get('statusValue').setValue(this.currentStatus.statusValue);
    this.registerForm.get('feedback').setValue(this.currentStatus.feedback);

    this.modalService.open(content, {size: 'lg', centered: true}).result.then((result) => {
    }, (reason) => {
      Object.assign(this.currentStatus, this.statusOld);
      this.alertService.createAlert(AlertType.INFO, 'Status not updated.');
      this.submitted = false;
    });
  }

  onSubmit(modal: NgbActiveModal) {
    this.internalError = null;
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }

    this.currentStatus.feedback = this.registerForm.value.feedback;

    const status: Status = new Status();
    status.id = this.currentStatus.id;
    status.task = this.currentTask;
    status.date = this.currentStatus.date;
    status.weekNumber = this.currentStatus.weekNumber;
    status.statusValue = this.currentStatus.statusValue;
    status.feedback = this.currentStatus.feedback;

    if (this.currentStatus.id) {
      this.statusService.update(status).pipe(first()).subscribe(() => {
        this.loadStatusList.emit(this.currentTask);
        this.submitted = false;
        modal.close();
        this.alertService.createAlert(AlertType.SUCCESS, 'Status updated.');
      }, err => {
        this.internalError = err;
      });
    } else {
      this.statusService.create(status).pipe(first()).subscribe(newStatus => {
        this.currentStatus = newStatus;
        this.loadStatusList.emit(this.currentTask);
        this.submitted = false;
        modal.close();
        this.alertService.createAlert(AlertType.SUCCESS, 'Status updated.');
      }, err => {
        this.internalError = err;
      });
    }
  }

  updateStatusValue(statusValue: StatusValue): void {
    this.currentStatus.statusValue = statusValue;
  }
}

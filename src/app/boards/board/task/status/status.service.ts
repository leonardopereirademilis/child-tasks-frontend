import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {Status} from './status';
import {APP_CONFIG, AppConfig} from '../../../../app.config';

@Injectable({
  providedIn: 'root'
})
export class StatusService {
  apiUrl: string;

  constructor(private http: HttpClient,
              private router: Router,
              @Inject(APP_CONFIG) config: AppConfig) {
    this.apiUrl = config.apiUrl;
  }

  getAll() {
    return this.http.get<Status[]>(`${this.apiUrl}/status`);
  }

  getById(id: string) {
    return this.http.get<Status>(`${this.apiUrl}/status/${id}`);
  }

  getAllByTaskIdAndWeekNumber(id: string, weekNumber: number) {
    return this.http.get<Status[]>(`${this.apiUrl}/status/task/${id}/week/${weekNumber}`);
  }

  getAllByTaskId(id: string) {
    return this.http.get<Status[]>(`${this.apiUrl}/status/task/${id}`);
  }

  create(status: Status) {
    return this.http.post<Status>(`${this.apiUrl}/status`, status);
  }

  update(status: Status) {
    return this.http.put(`${this.apiUrl}/status/${status.id}`, status);
  }

  delete(id: string) {
    return this.http.delete(`${this.apiUrl}/status/${id}`);
  }

  updateStatus(statusId: string) {
    this.router.navigate(['status', statusId]);
  }
}

﻿import {Task} from '../task';

export class Status {
  _id: string;
  id: string;
  task: Task;
  date: Date;
  weekNumber: number;
  statusValue: StatusValue;
  feedback: string;
}

export enum StatusValue {
  SUCCESS = 'SUCCESS',
  WARNING = 'WARNING',
  DANGER = 'DANGER'
}

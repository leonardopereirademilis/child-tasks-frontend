import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {APP_CONFIG, AppConfig} from '../../../app.config';
import {Task} from './task';
import {TaskType} from './taskType';

@Injectable({
  providedIn: 'root'
})
export class TaskService {
  apiUrl: string;

  constructor(private http: HttpClient,
              private router: Router,
              @Inject(APP_CONFIG) config: AppConfig) {
    this.apiUrl = config.apiUrl;
  }

  getAll() {
    return this.http.get<Task[]>(`${this.apiUrl}/tasks`);
  }

  getAllTaskType() {
    return this.http.get<TaskType[]>(`${this.apiUrl}/tasks/task-type`);
  }

  getById(id: string) {
    return this.http.get<Task>(`${this.apiUrl}/tasks/${id}`);
  }

  getAllByBoardId(id: string) {
    return this.http.get<Task[]>(`${this.apiUrl}/tasks/board/${id}`);
  }

  create(task: Task) {
    return this.http.post(`${this.apiUrl}/tasks`, task);
  }

  update(task: Task) {
    return this.http.put(`${this.apiUrl}/tasks/${task.id}`, task);
  }

  delete(id: string) {
    return this.http.delete(`${this.apiUrl}/tasks/${id}`);
  }

  updateTask(taskId: string) {
    this.router.navigate(['task', taskId]);
  }

}

﻿import {Board} from '../board';
import {TaskType} from './taskType';
import {Status} from './status/status';

export class Task {
  id: string;
  board: Board;
  taskType: TaskType = null;
  taskName: string;
  statusList: Status[];
  sunday = true;
  monday = true;
  tuesday = true;
  wednesday = true;
  thursday = true;
  friday = true;
  saturday = true;
  startDate: Date;
  endDate: Date;
  prize: number;
  disabled = false;
  taskGoal = 80;
}

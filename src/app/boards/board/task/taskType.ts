﻿export class TaskType {
  id: string;
  taskType: string;
  taskTypeName: string;
  taskIcon: string;
  useTaskName: boolean;
}

import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-trophy',
  templateUrl: './trophy.component.html',
  styleUrls: ['./trophy.component.css']
})
export class TrophyComponent implements OnInit {

  // tslint:disable-next-line:variable-name
  _currentRate = 0;
  prize = 0;
  height = 65;

  @Input() disabled = false;

  get currentRate(): number {
    return this._currentRate;
  }

  @Input()
  set currentRate(value: number) {
    this._currentRate = value;
    this.setPrize();
  }

  constructor() {
  }

  ngOnInit() {
    this.setPrize();
  }

  setPrize() {
    this.prize = Math.round(this._currentRate * 100);
    this.height = 65 - (65 * this._currentRate);
  }

}

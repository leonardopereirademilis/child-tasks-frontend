import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Board} from './board/board';
import {UserService} from '../users/user.service';
import {BoardService} from './board.service';
import {first} from 'rxjs/operators';
import * as moment from 'moment';
import {NewBoardComponent} from './new-board/new-board.component';
import {AlertService, AlertType} from '../_alert/alert.service';
import {CheckoutService} from '../checkout/checkout.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-boards',
  templateUrl: './boards.component.html',
  styleUrls: ['./boards.component.css']
})
export class BoardsComponent implements OnInit {
  cancelForm: FormGroup;
  submitted = false;

  @ViewChild('content', {static: false}) content: ElementRef;
  @ViewChild('newBoardComponent', {static: false}) newBoardComponent: NewBoardComponent;

  constructor(private userService: UserService,
              private formBuilder: FormBuilder,
              private modalService: NgbModal,
              private alertService: AlertService,
              private checkoutService: CheckoutService,
              public boardService: BoardService) {
  }

  ngOnInit(): void {
    this.loadAllBoards();
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.cancelForm.controls;
  }

  loadAllBoards() {
    this.boardService.getAllByUserId(this.userService.currentUser._id).pipe(first()).subscribe(boards => {
      this.boardService.myBoards = boards;
      this.boardService.myBoards.forEach(board => {
        this.checkoutService.getAllByBoardId(board.id).pipe(first()).subscribe(checkouts => {
          board.checkouts = checkouts;
        });
      });
    });
  }

  getAge(date: Date): number {
    return moment().diff(date, 'years');
  }

  editBoard(board: Board) {
    this.newBoardComponent.editBoard(board);
  }

  deleteBoard(board: Board) {
    this.boardService.delete(board.id).pipe(first()).subscribe(() => {
      this.loadAllBoards();
      this.alertService.createAlert(AlertType.SUCCESS, 'Board deleted.');
    }, err => {
      this.alertService.createAlert(AlertType.DANGER, 'Board not deleted. Error: ' + err);
    });
  }

  isAvailable(board: Board): boolean {
    return new Date(board.availableEndDate) >= new Date();
  }

  getCheckoutStatus(board: Board): string {
    let checkoutStatus = '';
    if (!board.checkouts || board.checkouts.length === 0) {
      checkoutStatus = 'NOT_PAYED';
    } else {
      const checkout = board.checkouts[board.checkouts.length - 1];
      checkoutStatus = checkout.status;
    }

    return checkoutStatus;
  }

  openCancelPayment(board: Board) {
    this.modalService.dismissAll();
    this.submitted = false;

    this.cancelForm = this.formBuilder.group({
      cancel: ['', Validators.required],
      feedback: ['', Validators.required],
      boardId: ['', Validators.required]
    });

    this.cancelForm.get('cancel').setValue(false);
    this.cancelForm.get('feedback').setValue('');
    this.cancelForm.get('boardId').setValue(board.id);

    this.modalService.open(this.content, {size: 'lg', centered: true}).result.then((result) => {
    }, (reason) => {
      this.alertService.createAlert(AlertType.INFO, 'Payment not cancelled.');
      this.submitted = false;
    });
  }

  onSubmit(modal: NgbActiveModal) {
    this.submitted = true;

    // stop here if form is invalid
    if (this.cancelForm.invalid || !this.cancelForm.value.cancel) {
      return;
    }

    this.checkoutService.cancelPayment(this.cancelForm.value.boardId, this.cancelForm.value.feedback).pipe(first()).subscribe(() => {
      this.submitted = false;
      this.loadAllBoards();
      modal.close();
      this.alertService.createAlert(AlertType.SUCCESS, 'Payment cancelled.');
    }, err => {
      this.alertService.createAlert(AlertType.DANGER, err);
    });
  }
}

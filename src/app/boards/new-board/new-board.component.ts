import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {
  NgbActiveModal,
  NgbDateParserFormatter,
  NgbDatepickerI18n,
  NgbDateStruct,
  NgbModal
} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {first} from 'rxjs/operators/first';
import {UserService} from '../../users/user.service';
import {Board} from '../board/board';
import {BoardService} from '../board.service';
import {Router} from '@angular/router';
import {I18n, NgbDatepickerI18nBR} from '../../_helpers/ngb-datepicker-i18n-br';
import {NgbDateBRParserFormatter} from '../../_helpers/ngb-date-br-parser-formatter';
import {AlertService, AlertType} from '../../_alert/alert.service';
import {CheckoutComponent} from '../../checkout/checkout.component';

@Component({
  selector: 'app-new-board',
  templateUrl: './new-board.component.html',
  styleUrls: ['./new-board.component.css'],
  providers: [
    I18n,
    {provide: NgbDatepickerI18n, useClass: NgbDatepickerI18nBR},
    {provide: NgbDateParserFormatter, useClass: NgbDateBRParserFormatter}
  ]
})
export class NewBoardComponent implements OnInit {
  today: NgbDateStruct;
  birthDateStartDate: NgbDateStruct;
  startDateStartDate: NgbDateStruct;
  registerForm: FormGroup;
  newBoard: Board = new Board();
  submitted = false;
  internalError: string = null;
  isEditing = false;

  @Input() editButton = false;
  @Output() loadAllBoards = new EventEmitter();

  @ViewChild('content', {static: false}) content: ElementRef;
  @ViewChild('checkoutComponent', {static: false}) checkoutComponent: CheckoutComponent;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private modalService: NgbModal,
              private alertService: AlertService,
              public userService: UserService,
              public boardService: BoardService) {
  }

  ngOnInit() {
    const now = new Date();
    this.today = {year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate()};
    this.birthDateStartDate = {year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate()};
    this.startDateStartDate = {year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate()};
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.registerForm.controls;
  }

  editBoard(board: Board) {
    this.isEditing = true;
    this.newBoard = board;
    this.open(this.content, board.test);
  }

  open(content: any, testBoard: boolean) {
    this.modalService.dismissAll();
    this.submitted = false;
    this.internalError = null;

    this.registerForm = this.formBuilder.group({
      childName: ['', Validators.required],
      childBirthDate: ['', Validators.required],
      childGender: ['', Validators.required],
      startDate: ['', Validators.required],
      accept: ['', Validators.required],
      boardGoal: ['', [Validators.required, Validators.min(1), Validators.max(100)]]
    });

    if (!this.isEditing) {
      this.newBoard = new Board();
    }

    this.newBoard.user = this.userService.currentUser;
    this.newBoard.test = testBoard;

    this.registerForm.get('childName').setValue(this.newBoard.childName);
    this.registerForm.get('boardGoal').setValue(this.newBoard.boardGoal);

    let childBirthDate: NgbDateStruct = null;
    if (this.newBoard.childBirthDate) {
      childBirthDate = {
        year: new Date(this.newBoard.childBirthDate).getFullYear(),
        month: new Date(this.newBoard.childBirthDate).getMonth() + 1,
        day: new Date(this.newBoard.childBirthDate).getDate()
      };
      this.birthDateStartDate = childBirthDate;
    }
    this.registerForm.get('childBirthDate').setValue(childBirthDate);
    this.registerForm.get('childGender').setValue(this.newBoard.childGender);

    let startDate: NgbDateStruct = null;
    if (this.newBoard.startDate) {
      startDate = {
        year: new Date(this.newBoard.startDate).getFullYear(),
        month: new Date(this.newBoard.startDate).getMonth() + 1,
        day: new Date(this.newBoard.startDate).getDate()
      };
      this.startDateStartDate = startDate;
    }
    this.registerForm.get('startDate').setValue(startDate);

    this.modalService.open(content, {size: 'lg', centered: true}).result.then((result) => {
    }, (reason) => {
      if (this.isEditing) {
        this.alertService.createAlert(AlertType.INFO, 'Board not updated.');
      } else {
        this.alertService.createAlert(AlertType.INFO, 'Board not created.');

      }
      this.submitted = false;
      this.isEditing = false;
    });
  }

  onSubmit(modal: NgbActiveModal) {
    this.internalError = null;
    this.submitted = true;

    if (this.newBoard.test || this.isEditing) {
      this.registerForm.get('accept').setValue(true);
    }

    // stop here if form is invalid
    if (this.registerForm.invalid || !this.registerForm.value.accept) {
      return;
    }

    this.newBoard.user = this.userService.currentUser;
    this.newBoard.childName = this.registerForm.value.childName;
    this.newBoard.boardGoal = this.registerForm.value.boardGoal;
    this.newBoard.childGender = this.registerForm.value.childGender;

    if (this.isEditing) {
      this.boardService.update(this.newBoard).pipe(first()).subscribe(() => {
        this.submitted = false;
        this.isEditing = false;
        this.loadAllBoards.emit();
        modal.close();
        this.alertService.createAlert(AlertType.SUCCESS, 'Board updated.');
      }, err => {
        this.internalError = err;
      });
    } else {
      this.boardService.create(this.newBoard).pipe(first()).subscribe(newBoard => {
        this.newBoard = newBoard;
        this.submitted = false;
        this.isEditing = false;
        if (!this.newBoard.test) {
          this.checkoutComponent.board = this.newBoard;
          this.checkoutComponent.submit();
        }
        this.loadAllBoards.emit();
        modal.close();
        this.alertService.createAlert(AlertType.SUCCESS, 'Board created.');
      }, err => {
        this.internalError = err;
      });
    }
  }

  updateChildBirthDate(): void {
    this.newBoard.childBirthDate = new Date(this.registerForm.value.childBirthDate.year,
      this.registerForm.value.childBirthDate.month - 1,
      this.registerForm.value.childBirthDate.day,
      0,
      0,
      0,
      0);
  }

  updateStartDate(): void {
    this.newBoard.startDate = new Date(this.registerForm.value.startDate.year,
      this.registerForm.value.startDate.month - 1,
      this.registerForm.value.startDate.day,
      0,
      0,
      0,
      0);
  }

  testAvailable(): boolean {
    const boards = this.boardService.myBoards.filter(board => board.test);
    return (!(boards && boards.length > 0));
  }

}

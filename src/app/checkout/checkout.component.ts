import {Component, ElementRef, Inject, Input, OnInit, ViewChild} from '@angular/core';
import {CheckoutService} from './checkout.service';
import {first} from 'rxjs/operators';
import {APP_CONFIG, AppConfig} from '../app.config';
import {Checkout} from './checkout';
import {UserService} from '../users/user.service';
import {AlertService, AlertType} from '../_alert/alert.service';
import {Board} from '../boards/board/board';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {

  checkout: Checkout;
  pagseguroUrl: string;
  MON3_BRL10: string;
  @Input() thisBoard = false;
  @Input() board: Board;
  @Input() buttomLabel: string;
  code: string;

  @ViewChild('form', {static: false}) form: ElementRef;

  constructor(private checkoutService: CheckoutService,
              private userService: UserService,
              private alertService: AlertService,
              @Inject(APP_CONFIG) config: AppConfig) {
    this.pagseguroUrl = config.pagseguroUrl;
    this.MON3_BRL10 = config.MON3_BRL10;
  }

  ngOnInit() {
    this.code = this.MON3_BRL10;
  }

  submit() {
    this.checkout = new Checkout();
    this.checkout.user = this.userService.currentUser;
    this.checkout.board = this.board;
    this.checkout.code = this.code;
    this.checkout.date = new Date();
    this.checkoutService.create(this.checkout).pipe(first()).subscribe(checkout => {
        this.checkout = checkout;
        this.form.nativeElement.submit();
      },
      error => {
        this.alertService.createAlert(AlertType.DANGER, error);
      });
  }
}

import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {APP_CONFIG, AppConfig} from '../app.config';
import {Checkout} from './checkout';

@Injectable({
  providedIn: 'root'
})
export class CheckoutService {
  apiUrl: string;

  constructor(private http: HttpClient,
              private router: Router,
              @Inject(APP_CONFIG) config: AppConfig) {
    this.apiUrl = config.apiUrl;
  }

  getAll() {
    return this.http.get<Checkout[]>(`${this.apiUrl}/checkouts`);
  }

  getById(id: string) {
    return this.http.get<Checkout>(`${this.apiUrl}/checkouts/${id}`);
  }

  getAllByBoardId(boardId: string) {
    return this.http.get<Checkout[]>(`${this.apiUrl}/checkouts/board/${boardId}`);
  }

  create(checkout: Checkout) {
    return this.http.post<Checkout>(`${this.apiUrl}/checkouts`, checkout);
  }

  update(checkout: Checkout) {
    return this.http.put(`${this.apiUrl}/checkouts/${checkout.id}`, checkout);
  }

  delete(id: string) {
    return this.http.delete(`${this.apiUrl}/checkouts/${id}`);
  }

  cancelPayment(boardId: number, feedback: string) {
    return this.http.post(`${this.apiUrl}/checkouts/cancelPayment`, {boardId, feedback});
  }

  listPreApprovals(id: string) {
    return this.http.get<string>(`${this.apiUrl}/checkouts/listPreApprovals/${id}`);
  }
}

﻿import {User} from '../users/user';
import {Board} from '../boards/board/board';

export class Checkout {
  id: string;
  user: User;
  board: Board;
  code: string;
  date: Date;
  status: string;
}

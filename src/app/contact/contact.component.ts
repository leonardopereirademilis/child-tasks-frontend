import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AlertService, AlertType} from '../_alert/alert.service';
import {ContactService} from './contact.service';
import {first} from 'rxjs/operators';
import {Contact} from './contact';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  contactForm: FormGroup;
  submitted = false;

  constructor(private formBuilder: FormBuilder,
              private contactService: ContactService,
              private alertService: AlertService) {
  }

  ngOnInit() {
    this.contactForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      phone: ['', [Validators.required, Validators.minLength(6)]],
      message: ['', [Validators.required]]
    });
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.contactForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.contactForm.invalid) {
      return;
    }

    const contact = new Contact();
    contact.firstName = this.contactForm.value.firstName;
    contact.lastName = this.contactForm.value.lastName;
    contact.email = this.contactForm.value.email;
    contact.phone = this.contactForm.value.phone;
    contact.message = this.contactForm.value.message;

    this.contactService.send(contact).pipe(first()).subscribe(() => {
      this.submitted = false;
      this.contactForm.get('firstName').setValue(null);
      this.contactForm.get('lastName').setValue(null);
      this.contactForm.get('email').setValue(null);
      this.contactForm.get('phone').setValue(null);
      this.contactForm.get('message').setValue(null);

      this.alertService.createAlert(AlertType.SUCCESS, 'Message sent.');
    }, err => {
      this.alertService.createAlert(AlertType.DANGER, err);
    });


  }
}

import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {Contact} from './contact';
import {APP_CONFIG, AppConfig} from '../app.config';

@Injectable({
  providedIn: 'root'
})
export class ContactService {
  apiUrl: string;

  constructor(private http: HttpClient,
              private router: Router,
              @Inject(APP_CONFIG) config: AppConfig) {
    this.apiUrl = config.apiUrl;
  }

  send(contact: Contact) {
    return this.http.post(`${this.apiUrl}/contact`, contact);
  }

}

import {isPlatformBrowser} from '@angular/common';
import {HttpClient} from '@angular/common/http';
import {Inject, Injectable, PLATFORM_ID} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {map} from 'rxjs/operators/map';
import {APP_CONFIG, AppConfig} from '../app.config';
import {User} from './user';
import {AuthService, SocialUser} from 'angularx-social-login';
import {AlertService, AlertType} from '../_alert/alert.service';
import {Router} from '@angular/router';
import {UserService} from './user.service';
import {first} from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private readonly apiUrl: string;
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  constructor(private http: HttpClient,
              private router: Router,
              private authService: AuthService,
              private userService: UserService,
              private alertService: AlertService,
              @Inject(APP_CONFIG) config: AppConfig,
              @Inject(PLATFORM_ID) private platformId: any,
              @Inject('LOCALSTORAGE') private localStorage: any) {
    this.apiUrl = config.apiUrl;
    if (isPlatformBrowser(this.platformId)) {
      this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
      this.currentUser = this.currentUserSubject.asObservable();
    }
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  login(username: string, password: string) {
    return this.authenticate(username, password)
      .pipe(map(user => {
        // login successful if there's a jwt token in the response
        if (user && user.token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(user));
          this.currentUserSubject.next(user);
        }

        return user;
      }));
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
    this.authService.signOut();
  }

  authenticate(username: string, password: string) {
    return this.http.post<any>(`${this.apiUrl}/users/authenticate`, {username, password});
  }

  authenticateSocialUser(socialUser: SocialUser) {
    return this.http.post<any>(`${this.apiUrl}/users/authenticateSocialUser`, socialUser);
  }

  socialSignIn(providerId: string, returnUrl: string) {
    this.authService.signIn(providerId)
      .then(socialUser => {
        this.authenticateSocialUser(socialUser)
          .pipe(first())
          .subscribe(
            user => {
              if (user && user.token) {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('currentUser', JSON.stringify(user));
                this.currentUserSubject.next(user);
              }
              this.router.navigate([returnUrl]);
            },
            error => {
              this.alertService.createAlert(AlertType.DANGER, error);
            });
      })
      .catch(err => {
        this.alertService.createAlert(AlertType.DANGER, err);
      });
  }


}

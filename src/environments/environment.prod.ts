export const environment = {
  production: true,
  envName: 'prod',
  APP_CONFIG_IMPL: {
    apiUrl: 'https://www.quadrodetarefas.com.br:3000',
    pagseguroUrl: 'https://pagseguro.uol.com.br',
    MON3_BRL10: 'DDEEC6A78888415664AB4FB7C9B8230C'

  }
};

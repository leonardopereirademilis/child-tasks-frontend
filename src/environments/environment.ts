export const environment = {
  production: false,
  envName: 'dev',
  APP_CONFIG_IMPL: {
    apiUrl: 'http://localhost:3000',
    pagseguroUrl: 'https://sandbox.pagseguro.uol.com.br',
    MON3_BRL10: '51DE0FBA6D6DFC8114D6AFAD2D6DDD6A'
  }
};
